<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>UTS Nomer 3</title>
</head>

<body>

    <h1>Array</h1>
    <?php
    $name = array(
        array("Kids", "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"),
        array("Adults", "Hopper", "Nancy", "Joyce", "Jonathan", "Murray")
    );

    echo $name[0][0] . " : " . $name[0][1] . ", " . $name[0][2] . ", " .  $name[0][3] . ", " . $name[0][4] . ", " . $name[0][5] . ", " . $name[0][6] . ".<br>";
    echo $name[1][0] . " : " . $name[1][1] . ", " . $name[1][2] . ", " .  $name[1][3] . ", " . $name[1][4] . ", " . $name[1][5] . ".<br>";
    ?>

</body>

</html>