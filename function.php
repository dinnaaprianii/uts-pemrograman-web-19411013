<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UTS Nomer 5</title>
</head>

<body>
    <h1>Hasil Nilai</h1>
    <?php

    function tentukanNilai($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik<br>";
        }
        if ($nilai >= 70 && $nilai < 85) {
            return "Baik<br>";
        }
        if ($nilai >= 60 && $nilai < 70) {
            return "Cukup<br>";
        }
        return "Kurang";
    }

    echo tentukanNilai(98);
    echo tentukanNilai(76);
    echo tentukanNilai(67);
    echo tentukanNilai(43);


    ?>
</body>

</html>