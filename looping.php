<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UTS Nomer 4</title>
</head>
<body>
    <h1>Looping</h1>
    <h3>Looping Pertama</h3>
    <div class="loop1"></div>
    <?php
    for ($i=2;$i<=20;$i+=2){
        echo $i." - I Love PHP <br>";

    }
    ?>
    <br>
    <h3>Looping Kedua</h3>
    <div class="loop2"></div>
    <?php
    for ($i=20;$i>0;$i-=2){
        echo $i." - I Love PHP <br>";
        
    }
    ?>
</body>
</html>